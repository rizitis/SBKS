# SBKS  
### (Slack-Build-Kernel-Script) 
It use zenity dialogs, so zenity required (install it from SBo)

SBKS is a bash script for auto download, build, and install a second (backup) Linux kernel for Slackware and Slack based systems.
Slackware Based Distros should execute this script as root ONLY, NOT sudo ./SBKS else  mkinitrd probably will fail and SBKS will stop.

A 5 minutes video is here how to install and run SBKS

[![SBKS](https://i9.ytimg.com/vi_webp/PfLugpULA-8/mq1.webp?sqp=CPCY0J4G-oaymwEmCMACELQB8quKqQMa8AEB-AH-CYAC0AWKAgwIABABGB0gZShkMA8=&rs=AOn4CLCcT3zY0IaCGRkYtx6NC6369UT6EA)](https://www.youtube.com/watch?v=PfLugpULA-8)

### Install 
First install zenity from SlackBuild.org 
Use sbopkg or slpkg or whatever you like.

```
su -l or sudo -i
wget -P /usr/local/bin/ https://gitlab.com/rizitis/SBKS/-/raw/main/SBKS
chmod +x /usr/local/bin/SBKS
```


### Run SBKS 
```
su -l or sudo -i 
SBKS 
```

Everything else you will find it in the script at zenity dialogs.

### Info
SBKS wont remove or upgrade older Linux kernel versions that allready exist in your system.
 In case your old .config is a custom kernel, for example if you are running now a kernel builded by you, named 6.0.14-rtz (uname -r will show you)
 Then on the VERSION2 dialog ONLY you should write:
```
6.1.8-rtz
```
If for example the Kernel you want to build is 6.1.8...
AND you dont have custom kernel version, or you are using officially Slackware Kernel, then just write again on dialog box the kernel version you wise to build. 
```
6.1.8
```

With this script you can always have a second generic kernel that officially Slackware upgrade kernels will not touch it.
We dont need SBKS but only if we want a second or a third... kernel in our system. In other words, Slackware kernels are always fine, but just in case... 
It is tested to Slackware64 15+ and Slackel

### NOTE: 
This script its NOT working for testing kernels (for example 6.2-RC) 
For testing kernels use SBKS-RC (zenity not needed for this)
(https://github.com/rizitis/SBKS/tree/SBKS-RC)
